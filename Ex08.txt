O código não funciona ao realizar as modificações citadas. 
Isso ocorre porque Funcionario é uma classe, se comporta como sendo um tipo de dado. 
Logo, não podemos alterar as informações contidas em Funcionario, já que para armazenarmos novos dados precisamos alocar um espaço na memória.
Para fazermos o que o exercicio sugere é necessário criar um novo objeto dessa classe.

Logo, percebe-se que nada do que foi proposto pelo exercicio é coerente.
