public class Ex09_TestaFuncionario {

	public static void main(String[] args) {

	// Instanciando Classes
		Ex09_Funcionario f1 = new Ex09_Funcionario();
		Ex09_Data data= new Ex09_Data();
		f1.pontData(data);

	//Setando valores
		f1.setNome("Alexandre");
		f1.setSalario(100);
		f1.recebeAumento(50);
		f1.setDepartamento("Engenharia");
		data.setDia(1);
		data.setMes(1);
		data.setAno(2014);
		f1.setRG("111111");

	//Mostrando na tela
		f1.mostra();


	}
}
