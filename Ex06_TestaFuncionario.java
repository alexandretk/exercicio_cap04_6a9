/*
		ALTERACOES EXERCICIO 6


		Data data= new Data();

		f1.pontData(data); // Enviar o ponteiro do objeto data da classe Data para o objeto f1(classe funcionario).
*/

public class Ex06_TestaFuncionario {

	public static void main(String[] args) {

	// Instanciando Classes
		Ex06_Funcionario f1 = new Ex06_Funcionario();
		Ex06_Data data= new Ex06_Data();
		f1.pontData(data);

	//Setando valores
		f1.setNome("Alexandre");
		f1.setSalario(100);
		f1.recebeAumento(50);
		f1.setDepartamento("Engenharia");
		data.setDia(1);
		data.setMes(1);
		data.setAno(2014);
		f1.setRG("111111");

	//Mostrando na tela
		f1.mostra();
		
		System.out.println("Data de entrada: " + Integer.toString(data.getDia()) +"/" + Integer.toString(data.getMes()) + "/"+ Integer.toString(data.getAno())+ "\n");

	}
}
