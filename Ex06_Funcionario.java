/* 
			Mudancas efetudadas no exercicio 6


	private Data dataEntrada;


	public void pontData(Data data) {
	dataEntrada=data;
}


*/

public class Ex06_Funcionario {
//Chamar a classe de Funcionario

	private String nome;
	private String departamento;
	private double salario;
	private Ex06_Data dataEntrada;
	private String rg;



	public void recebeAumento (double aumento) {
		salario+= aumento;
	}

	public double calculaGanhoAnual () {
		return 12*salario;
	}

// Metodo MOSTRA atributos na tela


public void mostra() {
System.out.println(" Nome do funcionario: " + nome);
System.out.println(" Departamento do funcionario: " + departamento);
System.out.println(" Salario do Funcionario: " + salario);
// Correcao do mostra sera feita no exercicio 7
//System.out.println(" Data de entrada no banco: " + dataEntrada); 
System.out.println(" RG do funcionario: " + rg);
}


// Setters e Getters
	public void setNome(String nome){
		this.nome=nome;
	}

	public String getNome() {
		return nome;
	}

	public void setDepartamento(String departamento) {
		this.departamento=departamento;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setSalario(double salario){
		this.salario=salario;
	}

	public double getSalario() {
		return salario;
	}
/*
	public void setDataEntrada(String dataEntrada){
		this.dataEntrada=dataEntrada;
	}

	public String getDataEntrada() {
		return dataEntrada;
	}
*/
	public void setRG(String rg){
		this.rg=rg;
	}

	public String getRG() {
		return rg;
	}

//Ponteiro para um objeto da classe data
	public void pontData(Ex06_Data data) {
	dataEntrada=data;
}



}
